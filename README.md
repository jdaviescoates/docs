This repository holds the Cloudron documentation, knowledge base and rest api docs

Live docs site: [https://docs.cloudron.io](https://docs.cloudron.io)

## Prerequisites

* mkdocs
* mkdocs-material
* mkdocs-redirects 
* @redocly/cli

Just run ./update.sh it will install the dependencies

## Preview

```
cd knowledgebase
mkdocs serve
```

## Build

```
mkdocs Build
````

## API Docs

```
redocly preview-docs api/cloudron-openapi.yaml
```
