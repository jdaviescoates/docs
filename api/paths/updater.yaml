definitions:
  BoxUpdateInfo:
    type: object
    description: Cloudron Update Information
    properties:
      version:
        type: string
        description: The SemVer Version of the next Cloudron release
      changelog:
        type: array
        description: Array of change log lines
        items:
          type: string
      sourceTarballUrl:
        type: string
        description: URL to download release tarball
      sourceTarballSigUrl:
        type: string
        description: URL to download release tarball signature. The GPG public key is stored in `src/releases.gpg` of Cloudron git repo.
      boxVersionsUrl:
        type: string
        description: URL to download release versions file
      boxVersionsSigUrl:
        type: string
        description: URL to download release versions file signature. The GPG public key is stored in `src/releases.gpg` of Cloudron git repo.
      unstable:
        type: boolean
        description: Whether this release is considered stable or not

  AppUpdateInfo:
    type: object
    description: App Update Information
    properties:
      id:
        type: string
        description: The App Store Id
        example: "org.wordpress.cloudronapp"
      creationDate:
        type: string
        description: Date when this app version was created
        example: "2023-08-30T11:33:42.000Z"
      publishState:
        type: string
        description: Approval status of the app. Usually `approved`.
      ownerId:
        type: string
        description: Cloudron.io account owner id
      manifest:
        type: object
        description: Cloudron [App Manifest](https://docs.cloudron.io/packaging/manifest/)
      iconUrl:
        type: string
        description: URL to download the app icon for this version
      unstable:
        type: boolean
        description: Whether this release is considered stable or not

  UpdateInfo:
    type: object
    description: Update Information
    properties:
      box:
        $ref: '#/definitions/BoxUpdateInfo'
    additionalProperties:
      # see https://github.com/Redocly/redoc/issues/622 and https://github.com/OAI/OpenAPI-Specification/issues/2294
      x-additionalPropertiesName: appId
      $ref: '#/definitions/AppUpdateInfo'

  CronPattern:
    type: string
    description: >
      Automatic update pattern in [cron](https://crontab.guru/) format.
    example: "00 00 1,3,5,23 * * *"

/updater/updates:
  get:
    operationId: getUpdates
    summary: Get Pending Updates
    description: >
      Gets information of any pending update to apps and the platform. This endpoint returns the cached update
      information and does not contact cloudron.io App Store. The update information could be stale and if you
      want the latest information use the [checkForUpdates](#operation/checkForUpdates) endpoint.
    tags: [ "Updater" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                updates:
                  $ref: '#/definitions/UpdateInfo'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/updater/updates"

/updater/check_for_updates:
  post:
    operationId: checkForUpdates
    summary: Check for updates
    description: >
      Gets information of any pending update to apps and the platform by contacting cloudron.io App Store. This call can take a while
      since it has to get the update information of each installed app. Use the [getUpdates](#operation/getUpdates) endpoint to get the
      update information since the last check.
    tags: [ "Updater" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                updates:
                  $ref: '#/definitions/UpdateInfo'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/updater/check_for_updates"

/updater/update:
  post:
    operationId: updateCloudron
    summary: Update Cloudron
    description: >
      Updates Cloudron to the next available release. The update is started asynchronously and can be monitored using the
      [tasks API](#tag/Tasks).
    tags: [ "Updater" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: false
      content:
        application/json:
          schema:
            type: object
            properties:
              skipBackup:
                type: boolean
                description: Whether backup should be skipped before performing the update.
                default: false
    responses:
      202:
        $ref: '../responses.yaml#/task_started'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      409:
        $ref: '../responses.yaml#/conflict'
      422:
        $ref: '../responses.yaml#/unprocessable_entity'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/updater/update"

/updater/autoupdate_pattern:
  get:
    operationId: getAutoupdatePattern
    summary: Get Update Schedule
    description: Get the automatic update schedule
    tags: [ "Updater" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                pattern:
                  $ref: '#/definitions/CronPattern'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/updater/autoupdate_pattern"

  post:
    operationId: setAutoupdatePattern
    summary: Set Update Schedule
    description: Sets the automatic update schedule
    tags: [ "Updater" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              pattern:
                $ref: '#/definitions/CronPattern'
            required:
              - pattern
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/updater/autoupdate_pattern" --data '{"pattern":"00 00 1,5 * * *"}'
