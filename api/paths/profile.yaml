definitions:
  Profile:
    type: object
    description: The user's profile
    properties:
      id:
        type: string
        description: Unique user id
        readOnly: true
      username:
        type: string
        description: Unique username
        readOnly: true
        example: messi
      email:
        type: string
        description: Unique email address
        example: goat@football.com
      fallbackEmail:
        type: string
        description: >
          Unique fallback email address. This is the address password reset is sent to. This requires the `password` field to
          be set to the user's current password.
        example: goat@soccer.com
      password:
        type: string
        description: When trying to change the `fallbackEmail`, set this field to the user's current password
        writeOnly: true
      displayName:
        type: string
        description: Full name of the user
        example: Lionel Messi
      twoFactorAuthenticationEnabled:
        type: boolean
        description: Whether user has enabled 2FA
        readOnly: true
      role:
        type: string
        enum: [owner,admin,usermanager,mailmanager,user]
        description: User's role determines the resources they have access to
        readOnly: true
      hasBackgroundImage:
        type: boolean
        description: Whether user has a custom dashboard background image
        readOnly: true
      avatarUrl:
        type: string
        description: >
          URL to user's avatar (icon) . This can be:
            * Empty string - if user has no icon set
            * Gravatar URL - external URL if user chose Gravatar
            * Custom Avatar URL - a link to the [custom avatar](#operation/getCustomAvatar) endpoint
        readOnly: true
      source:
        type: string
        description: Authentication source
        example: ldap
        readOnly: true

parameters:
  userId:
    name: userId
    in: path
    description: User Id
    required: true
    schema:
      type: string

/profile:
  get:
    operationId: getProfile
    summary: Get Profile
    description: Get the authenticated user's profile
    tags: [ "Profile" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              $ref: '#/definitions/Profile'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/profile"

  post:
    operationId: updateProfile
    summary: Update Profile
    description: Update user's profile information
    tags: [ "Profile" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#/definitions/Profile'
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/profile" --data '{"displayName": "Diego Maradona"}'

/profile/avatar:
  post:
    operationId: updateAvatar
    summary: Update Avatar (icon)
    description: Update user's avatar (icon)
    tags: [ "Profile" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        multipart/form-data:
          schema:
            type: object
            properties:
              avatar:
                type: string
                format: binary
            required:
              - avatar
        application/json:
          schema:
            type: object
            properties:
              avatar:
                type: string
                enum: [ '', 'gravatar']
                example: gravatar
            required:
              - avatar
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL (gravatar)
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/profile/avatar" --data '{"avatar": "gravatar"}'
      - lang: cURL (custom)
        source: |-
          curl -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/profile/avatar" --form avatar=@localfilename.png

/profile/avatar/{userId}:
  get:
    operationId: getCustomAvatar
    summary: Get Custom Avatar (icon)
    description: >
      Get the User's custom avatar (icon). This route is public. As a simple security measure, this requires the user id.

      To display the user's avatar , use the `avatarUrl` property of the [profile](#operation/getProfile) endpoint. The `avatarUrl`
      will link to this endpoint when the user has a custom icon.
    tags: [ "Profile" ]
    parameters:
        - $ref: '#/parameters/userId'
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/octet-stream:
            schema:
              type: string
              format: binary
      404:
        $ref: '../responses.yaml#/not_found'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl "https://$CLOUDRON_DOMAIN/api/v1/profile/avatar/$USER_ID"

/profile/background_image:
  get:
    operationId: getBackgroundImage
    summary: Get Background Image
    description: >
      Get the user's custom background image for the dashboard.

      Use the `hasBackgroundImage` property of the [profile](#operation/getProfile) endpoint to determine if the user
      has a custom background image.
    tags: [ "Profile" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          image/png:
            schema:
              type: string
              format: binary
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/profile/backgroundImage"

  post:
    operationId: setBackgroundImage
    summary: Set Background Image (icon)
    description: Set the user's background image for the dashboard
    tags: [ "Profile" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        multipart/form-data:
          schema:
            type: object
            properties:
              backgroundImage:
                type: string
                format: binary
            required:
              - backgroundImage
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL (set)
        source: |-
          curl -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/profile/background_image" --form backgroundImage=@localfilename.png
      - lang: cURL (clear)
        source: |-
          curl -X POST -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/profile/background_image" --form backgroundImage=

/profile/password:
  post:
    operationId: updatePassword
    summary: Update Password
    description: Update user's password
    tags: [ "Profile" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              password:
                type: string
                description: Current password
                example: "current_password"
              newPassword:
                type: string
                description: New password
                example: "new_password"
            required:
              - password
              - newPassword
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      412:
        $ref: '../responses.yaml#/precondition_failed'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/profile/password" --data '{"password": "old_password", "newPassword": "new_password"}'

/profile/twofactorauthentication_secret:
  post:
    operationId: setTwoFactorAuthenticationSecret
    summary: Set 2FA Secret
    description: >
      Sets 2FA Secret. Enabling 2FA is a two step process:
        * Call this endpoint to generate a secret. The response contains the secret and a qrcode
        * Call the [Enable 2FA](#operation/enableTwoFactorAuthentication) endpoint with a TOTP Token (generated off the above secret)

    tags: [ "Profile" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                secret:
                  type: string
                qrcode:
                  type: string
                  description: This is a `image/png` in `base64` format
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/profile/twofactorauthentication_secret"

/profile/twofactorauthentication_enable:
  post:
    operationId: enableTwoFactorAuthentication
    summary: Enable 2FA
    description: >
      Enables Two Factor Authentication. Enabling 2FA is a two step process:
        * Call the [Set 2FA Secret](#operation/setTwoFactorAuthenticationSecret) endpoint to generate a secret. The response contains the secret and a qrcode
        * Call this endpoint with a TOTP Token (generated off the above secret) to enable 2FA
    tags: [ "Profile" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              totpToken:
                type: string
                description: Current TOTP Token based off the [generate secret](#operation/setTwoFactorAuthenticationSecret)
            required:
              - totpToken
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      412:
        $ref: '../responses.yaml#/precondition_failed'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/profile/twofactorauthentication_enable" --data '{"totpToken": "576428"}'

/profile/twofactorauthentication_disable:
  post:
    operationId: disableTwoFactorAuthentication
    summary: Disable 2FA
    description: Disable 2FA
    tags: [ "Profile" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              password:
                type: string
                description: Current password
                example: "current_password"
            required:
              - password
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      412:
        $ref: '../responses.yaml#/precondition_failed'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/profile/twofactorauthentication_disable" --data '{"password": "current_password"}'
