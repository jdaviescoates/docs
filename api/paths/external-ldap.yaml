definitions:
  ExternalLdapConfig:
    type: object
    properties:
      provider:
        type: string
        description: External LDAP Provider
        enum: [ ad, cloudron, jumpcloud, okta, univention, other, noop]
        example: jumpcloud
      url:
        type: string
        description: Address of the Docker Registry
        example: docker.io
      baseDn:
        type: string
        description: Base DN for users
      usernameField:
        type: string
        description: LDAP Field to use as the username
      filter:
        type: string
        description: LDAP Filter to use for filtering users
      groupBaseDn:
        type: string
        description: Base DN for groups
      bindDn:
        type: string
        description: LDAP authentication username
      bindPassword:
        type: string
        description: LDAP authentication password
    required:
      - provider

/external_ldap/config:
  get:
    operationId: getExternalLdapConfig
    summary: Get External Directory Config
    description: Cloudron will synchronize users and groups from this external LDAP or ActiveDirectory server
    tags: [ "External LDAP" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              $ref: '#/definitions/ExternalLdapConfig'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/external_ldap/config"

  post:
    operationId: setExternalLdapConfig
    summary: Set External LDAP Config
    description: Cloudron will synchronize users and groups from this external LDAP or ActiveDirectory server
    tags: [ "External LDAP" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#/definitions/ExternalLdapConfig'
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/docker/registry_config" --data '{"provider":"digitalocean","serverAddress":"registry.digitalocean.com/cloudron","username":"username","password":"password"}'

/external_ldap/sync:
  post:
    operationId: syncExternalLdap
    summary: Synchronize
    description: Trigger synchronization with external LDAP server
    tags: [ "External LDAP" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    responses:
      202:
        $ref: '../responses.yaml#/task_started'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/external_ldap/sync"
