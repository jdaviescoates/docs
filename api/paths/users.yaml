definitions:
  User:
    description: User
    type: object
    properties:
      id:
        type: string
        description: User Id.
        example: "uid-3f07b9e7-8cf2-494b-95cc-93dd05fc5bbc"
      username:
        type: string
        description: >
          Unique username across the instance. The following usernames are reserved: `admin`, `no-reply`, `postmaster`, `mailer-daemon`, `root`, `admins`, `users`.
        example: helena
      email:
        type: string
        description: Primary email of user.
        example: example@cloudron.io
      fallbackEmail:
        type: string
        description: Email address for password reset, in case the primary is hosted on the Cloudron itself.
        example: "fallback@example.com"
      displayName:
        type: string
        description: Full name of the user.
        example: Helena Mamchuk
      groupIds:
        type: array
        items:
          type: string
          example: gid-3f07b9e7-8cf2-494b-95cc-93dd05fc5bbc
        description: Array of group ids the user is a member of.
      active:
        type: boolean
        description: Indicates if the user is active and can log into apps.
      source:
        type: string
        description: Indicating if the user is local (empty string) or from an external source like "ldap".
      role:
        type: string
        description: Role of the user. Can be `admin`, `user`, `usermanager`, `mailmanager` or `owner`.
      twoFactorAuthenticationEnabled:
        type: boolean
        description: Indicates if use has enabled two factor authentiation (2FA)
      inviteAccepted:
        type: boolean
        description: Indicates if user has finished the invitation to the Cloudron

parameters:
  userId:
    name: userId
    in: path
    description: User ID
    required: true
    schema:
      type: string

/users:
  get:
    operationId: getUsers
    summary: List users
    description: List users
    tags: [ "Users" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - $ref: '../parameters.yaml#/PaginationPage'
      - $ref: '../parameters.yaml#/PaginationPerPage'
      - name: search
        in: query
        description: Filter by users which match either username, email, displayName
        required: false
        schema:
          type: string
      - name: active
        in: query
        description: Filter by active or not active users. If not provided any state matches.
        required: false
        schema:
          type: boolean
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                users:
                  type: array
                  items:
                    $ref: '#/definitions/User'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/users"
  post:
    operationId: addUser
    summary: Add a user
    description: Add a user. The role 'user-manager' is required to make this api call.
    tags: [ "Users" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              email:
                type: string
                description: The [Primary email](https://docs.cloudron.io/profile/#primary-email).
              username:
                type: string
                description: The [Username for the user](https://docs.cloudron.io/profile/#username).
              password:
                type: string
                description: Password used for the user.
              displayName:
                type: string
                description: The [Display name for the user](https://docs.cloudron.io/profile/#display-name).
              role:
                type: string
                enum: [owner,admin,usermanager,mailmanager,user]
                description: See [user roles](https://docs.cloudron.io/user-management/#roles) for more information on each role.
              fallbackEmail:
                type: string
                description: The [Password Recovery E-Mail](https://docs.cloudron.io/profile/#password-recovery-email).
            required:
              - email
    responses:
      201:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                id:
                    $ref: '#/definitions/User/properties/id'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/users" --data '{"username":"Example","email":"example@example.com","password":"insertYourPassword","displayName":"Example", "role":"user","fallbackEmail":"fallback@test.com"}

/users/{userId}:
  get:
    operationId: getUserById
    summary: Get user
    description: Get user
    tags: [ "Users" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - $ref: '#/parameters/userId'
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              $ref: '#/definitions/User'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      404:
        $ref: '../responses.yaml#/not_found'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" https://$CLOUDRON_DOMAIN/api/v1/users/$USER_ID
  delete:
    operationId: deleteUserById
    summary: Delete user 
    description: >
      Delete a user.

      The role 'user-manager' is required to make this api call.
    tags: [ "Users" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    parameters:
      - $ref: '#/parameters/userId'
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      404:
        $ref: '../responses.yaml#/not_found'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X DELETE -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/users/$USER_ID"

/users/{userId}/role:
  put:
    operationId: setRole
    summary: Set a user's role
    description: >
      Set user role.

      Users can't set roles for themselves.

      The role 'user-manager' is required to make this api call.
    tags: ["Users"]
    parameters:
      - $ref: '#/parameters/userId'
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              role:
                type: string
                enum: [owner,admin,usermanager,mailmanager,user]
                description: See [user roles](https://docs.cloudron.io/user-management/#roles) for more information on each role.
            required:
              - user
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      404:
        $ref: '../responses.yaml#/not_found'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X PUT -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/users/$USER_ID/role" --data '{"role" : "user"}'

/users/{userId}/active:
  put:
    operationId: setUserActive
    summary: Set active
    description: >
      Set a user active or not

      A user can't set themselve as active or inactive.

      The role 'user-manager' is required to make this api call.
    tags: [ "Users" ]
    parameters:
      - $ref: '#/parameters/userId'
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              active:
                type: boolean
                description: Specify whether the user is active or not.
            required:
              - active
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      404:
        $ref: '../responses.yaml#/not_found'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X PUT -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/users/$USER_ID/active" --data '{"active" : true}'

/users/{userId}/profile:
  post:
    operationId: updateUser
    summary: Update user
    description: >
      Update user.

      Username can only be updated when the user has no existing username!
    tags: [ "Users" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    parameters:
      - $ref: '#/parameters/userId'
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              email:
                type: string
                description: Primary email for the user.
              displayName:
                type: string
                description: Display name for the user.
              fallbackEmail:
                type: string
                description: Incase the email is hosted on the same Cloudron, this email is used for password resets.
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      404:
        $ref: '../responses.yaml#/not_found'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/users/$USER_ID/profile" --data '{"email":"beispiel@example.com","displayName":"Testuser", "fallbackEmail":"fb@test.com"}'

/users/{userId}/password:
  post:
    operationId: setPassword
    summary: Set password
    description: Set password
    tags: [ "Users" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    parameters:
      - $ref: '#/parameters/userId'
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              password:
                type: string
                description: User's password.
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      404:
        $ref: '../responses.yaml#/not_found'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/users/$USER_ID/password" --data '{"password":"changeMe"}'

/users/{userId}/ghost:
  post:
    operationId: impersonateUser
    summary: Impersonate a user
    description: >
      Impersonate a user.

      You require the 'admin' role to use that call.

    tags: [ "Users" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    parameters:
      - $ref: '#/parameters/userId'
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              password:
                type: string
                description: Temporary password
              expiresAt:
                type: integer
                default: 6 * 60 * 60 * 1000; // 6 hours
                description: Time until password expires in miliseconds.
                example: 1725615073184
            required:
              - password
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      404:
        $ref: '../responses.yaml#/not_found'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/users/$USER_ID/ghost" --data '{"password":"tempPassword", "expiresAt":"1725615073184"}'

/users/{userId}/groups:
  put:
    operationId: setLocalGroup
    summary: Set user group
    description: >
      Set user group.

      The role 'user-manager' is required to make this api call.
    tags: ["Users"]
    parameters:
      - $ref: '#/parameters/userId'
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              groupIds:
                description: Array of groupIds
                type: array
                uniqueItems: true
                example: ['gid-9eb38938-95a7-4fc7-8c15-e02fa341cdae', 'gid-9ebewj349-95a7-4fc7-85fd-43278sdawae']
                items:
                  description: groupId
                  type: string
            required:
              - groupIds
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      404:
        $ref: '../responses.yaml#/not_found'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X PUT -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/users/$USER_ID/groups" --data '{"groupIds":["gid-9eb38938-95a7-4fc7-8c15-e02fa341cdae"]}'

/users/{userId}/password_reset_link:
  get:
    operationId: getPasswordResetLink
    summary: Get password reset link
    description: Get password reset link
    tags: [ "Users" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - $ref: '#/parameters/userId'
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                passwordResetLink:
                  type: string
                  example: "https://${CLOUDRON_DOMAIN}/passwordreset.html?resetToken=32c4d730c4e851b46c374d1691f4ec5189c0c016216db8d64dbec54863eb5ad4"
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/users/$USER_ID" 

/users/{userId}/send_password_reset_email:
  post:
    operationId: sendPasswordResetEmail
    summary: Send password reset email
    description: >
      Send password reset email.

      The role 'user-manager' is required to make this api call.
    tags: [ "Users" ]
    parameters:
      - $ref: '#/parameters/userId'
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              email:
                type: string
            required:
              - email
    responses:
      202:
        $ref: '../responses.yaml#/accepted'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      404:
        $ref: '../responses.yaml#/not_found'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X PUT -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/users/$USER_ID/send_password_reset_email" --data '{"email" : "example@example.org"}'

/users/{userId}/invite_link:
  get:
    operationId: getInviteLink
    summary: Get invite link
    description: Get invite link
    tags: [ "Users" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - $ref: '#/parameters/userId'
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                inviteLink:
                  type: string
                  example: "https://{$CLOUDRON_DOMAIN}/setupaccount.html?inviteToken=d00108026451e32ba73fd1e46dc5f6b23e67e966ca89f70c67dc940b04492f6f&email=api%40example.com&username=test&displayName=Testing%20API"
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/users/$USER_ID/invite_link"

/users/{userId}/send_invite_email:
  post:
    operationId: sendInviteEmail
    summary: Send invite email
    description: >
      Send invite email.

      The role 'user-manager' is required to make this api call.
    tags: [ "Users" ]
    parameters:
      - $ref: '#/parameters/userId'
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              email:
                type: string
                description: The email, that should receive the invite link.
            required:
              - email
    responses:
      202:
        $ref: '../responses.yaml#/accepted'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      404:
        $ref: '../responses.yaml#/not_found'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X PUT -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/users/$USER_ID/send_invite_email" --data '{"email" : "example@example.org"}'

/users/{userId}/twofactorauthentication_disable:
  post:
    operationId: disableTwofactorauthentication
    summary: Disable 2FA
    description: >
      Disable 2FA.

      The role 'user-manager' is required to make this api call.
    tags: [ "Users" ]
    parameters:
      - $ref: '#/parameters/userId'
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      404:
        $ref: '../responses.yaml#/not_found'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X PUT -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/users/$USER_ID/twofactorauthentication_disable"