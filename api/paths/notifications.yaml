definitions:
  Notification:
    description: Notification
    type: object
    properties:
      id:
        type: string
        description: Notification Id
        example: "50"
      eventId:
        type: string
        description: EventId from Eventlog
        example: 051ceb23-5003-4b03-b46e-521611b02a1c
      title:
        type: string
        description: Subject of the notification
        example: "Reboot Required"
      message:
        type: string
        description: Notification message in markdown format
        example: To finish ubuntu security updates, a reboot is necessary.
      creationTime:
        type: string
        description: Time when notification was created
        example: 2022-03-05T02:30:00.000Z
      acknowledged:
        type: boolean
        description: Whether notification was acknowledged by user or not
        example: false

parameters:
  notificationId:
    name: notificationId
    in: path
    description: Notification Id
    required: true
    schema:
      type: string

/notifications:
    get:
      operationId: getNotifications
      summary: List Notifications
      description: List notifications
      tags: [ "Notifications" ]
      security:
        - bearer_auth: [ 'read' ]
        - query_auth: [ 'read' ]
      parameters:
        - $ref: '../parameters.yaml#/PaginationPage'
        - $ref: '../parameters.yaml#/PaginationPerPage'
        - name: acknowledged
          in: query
          description: Filter by acknowledged status
          required: false
          schema:
            type: boolean
      responses:
        200:
          description: Success
          content:
            application/x-logs:
              schema:
                type: object
                properties:
                  notifications:
                    type: array
                    items:
                      $ref: '#/definitions/Notification'
        401:
          $ref: '../responses.yaml#/unauthorized'
        403:
          $ref: '../responses.yaml#/forbidden'
        500:
          $ref: '../responses.yaml#/server_error'
      x-codeSamples:
        - lang: cURL
          source: |-
            curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/notifications"

/notifications/{notificationId}:
  get:
      operationId: getNotification
      summary: Get Notification
      description: Get notification by ID
      tags: [ "Notifications" ]
      security:
        - bearer_auth: [ 'read' ]
        - query_auth: [ 'read' ]
      parameters:
        - $ref: '#/parameters/notificationId'
      responses:
        200:
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/definitions/Notification'
        401:
          $ref: '../responses.yaml#/unauthorized'
        403:
          $ref: '../responses.yaml#/forbidden'
        404:
          $ref: '../responses.yaml#/not_found'
        500:
          $ref: '../responses.yaml#/server_error'
      x-codeSamples:
        - lang: cURL
          source: |-
            curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/notifications/$NOTIFICATION_ID"

  post:
    operationId: updateNotification
    summary: Update Notification
    description: >
      Set notification acknowledged state. Unacknowledged notifications count appears in the navbar of the Dashboard.
    tags: [ "Notifications" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    parameters:
      - $ref: '#/parameters/notificationId'
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              acknowledged:
                type: boolean
                example: true
                description: Set notification to acknowledged or not
            required:
              - acknowledged
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      404:
        $ref: '../responses.yaml#/not_found'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/notifications/$NOTIFICATION_ID" --data '{"acknowledged":true}'
