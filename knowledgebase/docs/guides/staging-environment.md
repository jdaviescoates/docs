
## Staging environment

When doing website development, it is useful to have production and staging environments. Cloudron's
backup and clone features can be used to create a workflow as follows:

* Install the app in `staging.example.com`. Do edits and development as desired.
* Once ready, make a 'snapshot' of the app by making an [app backup](/backups/#making-an-app-backup)
* Use the [clone UI](/backups/#clone-app) to install the app
  into `prod.example.com`.

The same mechanism above can be used bring latest production data to staging.


