# Upgrade PHP in LAMP App

## Overview

In this guide, we will see how to upgrade the PHP version of the LAMP app.

Cloudron runs apps as read-only containers for immutability and security reasons. For this reason,
it is not possible to "apt upgrade" on an installed LAMP app to update PHP. On Cloudron, you must
instead install a newer version of the LAMP app and import your existing app into it.

This guide can be used to upgrade from the LAMP 7.2 to LAMP 7.3 or from LAMP 7.2/7.3 to LAMP 7.4.

## Backup existing app

The first step is to take a backup of the existing app. Then, click on `Download Backup configuration`
to download the JSON file with the latest backup information.

<center>
<img src="/guides/img/app-backup-config.png" class="shadow" width="500px">
</center>

## Import backup

Install the latest version of the LAMP app from the App Store. You can install this in a new subdomain
like `next.domain.com`. Once installed, go to `Backups` section of the app and click on `Import`.

<center>
<img src="/guides/img/lamp-import.png" class="shadow" width="500px">
</center>

This will open a popup dialog. Upload the backup configuration that we downloaded in the previous step.

## Configure database credentials

On Cloudron, each app is sandboxed and has it's own database. This means that the new LAMP app we installed
will have a different database than our existing LAMP app. You will have to adjust the database credentials
that your app uses by using the File Manager or the Web Terminal. The new credentials are available in
`/app/data/credentials.txt`.

## Switch the app location

Once the app is satisfactorily running, it is time to relocate the new app to production. For this, first
move the old app to a new location like `old.domain.com` from the `Location` section. Then, move the new app
to the desired location.

<center>
<img src="/guides/img/lamp-relocate-old.png" class="shadow" width="500px">
</center>

We recommend keeping the old app for a couple of days before uninstalling it. For this, you can simply keep
the old app stopped (`Console` -> `Stop app`). This way, if your app is mis-behaving with a newer PHP version,
you can easily switch back.

