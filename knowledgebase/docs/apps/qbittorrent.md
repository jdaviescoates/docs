# <img src="/img/qbittorrent-logo.png" width="25px"> qBittorrent App

## About

qBittorrent is an advanced and multi-platform BitTorrent client.

* Questions? Ask in the [Cloudron Forum - qBittorent](https://forum.cloudron.io/category/177/qbittorrent)
* [qBittorrent Website](https://www.qbittorrent.org/)
* [Vuetorrent Website](https://github.com/WDaan/VueTorrent)

