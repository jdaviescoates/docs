# <img src="/img/jingo-logo.png" width="25px"> Jingo App

!!! warning "Discontinued"
    Please note this app is not available anymore since upstream development has stopped.

## About

The aim of Jingo Wiki is to provide an easy way to create a centralized documentation area for people used to work with git and markdown.


* Questions? Ask in the [Cloudron Forum - Jingo](https://forum.cloudron.io/category/83/jingo)
* [Jingo Website](https://github.com/claudioc/jingo)
* [Jingo issue tracker](https://github.com/claudioc/jingo/issues)

## Custom configuration

Use the [File manager](/apps#file-manager)
to place custom configuration under `/app/data/config.yml`.

See [Jingo docs](https://github.com/claudioc/jingo#configuration-options-reference)
for configuration options reference.

## Look and feel

You can add a sidebar, footer, custom CSS and custom Javascript by following the
instructions in [Jingo docs](https://github.com/claudioc/jingo#customization).

