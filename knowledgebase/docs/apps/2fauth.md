# <img src="/img/2fauth-logo.png" width="25px"> 2FAuth App

## About

2FAuth is a web based self-hosted alternative to One Time Passcode (OTP) generators like Google Authenticator, designed for both mobile and desktop.

* Questions? Ask in the [Cloudron Forum - 2FAuth](https://forum.cloudron.io/category/184/2fauth)
* [2FAuth Docs](https://docs.2fauth.app/)
* [2FAuth Discussions](https://github.com/Bubka/2FAuth/discussions)
* [2FAuth issue tracker](https://github.com/Bubka/2FAuth/issues)

## Custom Config

Custom config can be be placed in `/app/data/env`. See https://github.com/Bubka/2FAuth/blob/master/.env.example for various settings.

Be sure to restart the app after making any changes.

