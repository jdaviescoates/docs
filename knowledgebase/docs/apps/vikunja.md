# <img src="/img/vikunja-logo.png" width="25px"> Vikunja App

## About

Vikunja is the to-do app to organize your life.

* Questions? Ask in the [Cloudron Forum - Vikunja](https://forum.cloudron.io/category/127/vikunja)
* [Vikunja Website](https://vikunja.io)
* [Vikunja community](https://community.vikunja.io/)

## Config

Custom configuration can be set using the [File manager](/apps/#file-manager) by editing `/app/data/config.yml`.
The various options are documented [here](https://vikunja.io/docs/config-options/).

