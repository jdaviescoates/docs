# <img src="/img/calcom-logo.png" width="25px"> Cal.com App

## About

Cal.com is the event-juggling scheduler for everyone. Focus on meeting, not making meetings. Free for individuals.

* Questions? Ask in the [Cloudron Forum - Cal.com](https://forum.cloudron.io/category/174/cal-com)
* [Cal.com Website](https://cal.com/)
* [Cal.com Docs](https://cal.com/docs)
* [Cal.com Issue Tracker](https://github.com/calcom/cal.com/issues)

## Manage users

Managing users is an enterprise feature. As such, there is no way to list or delete users via the UI.

## Disable Registration

Registration can be enabled in the admin features page.

<center>
<img src="/img/calcom-disable-registration.png" class="shadow" width="500px">
</center>

## Custom config

Various [custom configuration](https://github.com/calcom/cal.com/blob/main/.env.example) can be set
by changing `/app/data/env` using the [File manager](/apps/#file-manager).

Be sure to restart the app after making any changes.

