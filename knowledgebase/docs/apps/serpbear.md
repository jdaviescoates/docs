# <img src="/img/serpbear-logo.png" width="25px"> SerpBear App

## About

SerpBear is an Open Source Search Engine Position Tracking App. It allows you to track your website's keyword positions in Google and get notified of their positions.

* Questions? Ask in the [Cloudron Forum - SerpBear](https://forum.cloudron.io/category/187/serpbear)
* [SerpBear Website](https://docs.serpbear.com/)
* [SerpBear issue tracker](https://github.com/towfiqi/serpbear/issues)
