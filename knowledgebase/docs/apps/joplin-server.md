# <img src="/img/joplin-server-logo.png" width="25px"> Joplin Server App

## About

Joplin is a free, open source note taking and to-do application, which can handle a large number of notes organised into notebooks.

* Questions? Ask in the [Cloudron Forum - Joplin Server](https://forum.cloudron.io/category/134/joplin-server)
* [Website](https://joplinapp.org/)
* [Issue tracker](https://github.com/laurent22/joplin/issues)
* [Upstream Forum](https://discourse.joplinapp.org/)

## Clients

Download the clients [here](https://joplinapp.org/download/).

## Custom configuration

Custom configuration can be set by editing `/app/data/config` using the [File manager](/apps/#file-manager).
See the list of configurable options [here](https://github.com/laurent22/joplin/blob/dev/packages/server/src/env.ts).

Be sure to restart the app after making changes.

