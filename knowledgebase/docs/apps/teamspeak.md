# <img src="/img/teamspeak-logo.png" height="32px" style="vertical-align: middle;"> TeamSpeak App

## About

Use crystal clear sound to communicate with your team mates cross-platform with military-grade security, lag-free performance & unparalleled reliability and uptime. 

* Questions? Ask in the [Cloudron Forum - Teamspeak Server](https://forum.cloudron.io/category/84/teamspeak)
* [Teamspeak Server Website](https://www.teamspeak.com)
* [Teamspeak Server forum](https://community.teamspeak.com/)

## Initial Setup

After installation, check the app logs (in the `Console` section) to get the admin server token. You can now connect
using one of the [Teamspeak client](https://www.teamspeak.com/en/downloads/).

On first time connect, the client with ask for a privilege key. This is the same as the admin server token.

<img src="/img/teamspeak-privilege-key.png" class="shadow" width="500px">

## License

To configure a Teamspeak license in an app instance, upload the license `.dat` file to `/app/data/licensekey.dat` using the web terminal or Cloudron cli tool, then restart the app from the dashboard.

