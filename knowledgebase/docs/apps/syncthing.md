# <img src="/img/syncthing-logo.png" width="25px"> Syncthing App

## About

Syncthing is a continuous file synchronization program. It synchronizes files between two or more computers in real time, safely protected from prying eyes.

* Questions? Ask in the [Cloudron Forum - Syncthing](https://forum.cloudron.io/category/56/syncthing)
* [Syncthing Website](https://syncthing.net)
* [Syncthing forum](https://forum.syncthing.net/)
* [Syncthing docs](https://docs.syncthing.net/)
* [Syncthing issue tracker](https://github.com/syncthing/syncthing/issues)

## Apps

Complete list of native GUIs and integrations is available [here](https://syncthing.net/).

* [Android app](https://play.google.com/store/apps/details?id=com.nutomic.syncthingandroid)

## Changing password

The Web UI password uses HTTP Basic Auth and can be changed from `Actions` dropdown -> `Advanced` -> `GUI`. Just
enter the desired password in the `Password` input box. Syncthing will automatically hash the password
and restart itself.

Note that there is [no logout functionality](https://github.com/syncthing/syncthing/issues/2669). To test
this new password, clear your browser cache.

  <center>
  <img src="/img/syncthing-password.png" class="shadow" width="500px">
  </center>


