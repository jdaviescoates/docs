# <img src="/img/castopod-logo.png" width="25px"> Castopod App

## About

Castopod is a free and open-source podcast hosting solution made for podcasters who want engage and interact with their audience.

* Questions? Ask in the [Cloudron Forum - Castopod](https://forum.cloudron.io/category/172/castopod)
* [Castopod Website](https://castopod.org/)
* [Castopod issue tracker](https://code.castopod.org/adaures/castopod/-/issues)

## Custom config

Custom configuration can be added in `/app/data/env` by using the [File Manager](/apps/#file-manager). The various
configurations options are documented [here](https://code.castopod.org/adaures/castopod/-/blob/develop/env).

Be sure to restart the app after making any changes.

