# <img src="/img/directus-logo.png" width="25px"> Directus App

## About

Directus is an Instant App & API for your SQL Database.

* Questions? Ask in the [Cloudron Forum - Directus](https://forum.cloudron.io/category/101/directus)
* [Directus Website](https://directus.io)
* [Directus issue tracker](https://github.com/directus/directus/issues)

## Login

### Cloudron Directory

By default, local users (including the auto-created admin user) are allowed to login along with Cloudron Directory users. To disable this, set the following in `/app/data/env.sh` using the [File manager](/apps/#file-manager) and restart the app.

```
export AUTH_DISABLE_DEFAULT="true" # no local user login
```

By default, Cloudron users are given the built-in `Adminstrator` role. You can create another role and assign this as the default instead. To do so, set the following in `/app/data/env.sh` using the [File manager](/apps/#file-manager) and restart the app.

```
export AUTH_LDAP_DEFAULT_ROLE_ID=<id of role>
```

## Environment variables

[Custom environment variables](https://docs.directus.io/self-hosted/config-options.html) can be set in `/app/data/env.sh` using the [File manager](/apps/#file-manager).

Be sure to restart the app after making any changes.

## Extensions

Directus has a number of [extensions](https://github.com/directus-community/awesome-directus#extensions).

On Cloudron, extensions are installed in the extensions folder. This is set to `/app/data/extensions`.

We have a helper script at `/app/pkg/install-extension.sh` to install extensions:

```
# /app/pkg/install-extension.sh directus-extension-wpslug-interface

added 1 package in 1s

Restart Directus to enable the extension
```

!!! note "Killed"
    If the script above ends with `Killed`, it means that it got killed by the out of memory killer.
    To fix this, increase the [memory limit](/apps/#memory-limit) of the app to say 2GB and re-run the script.

