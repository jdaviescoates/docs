# <img src="/img/xbackbone-logo.png" width="25px"> XBackBone App

## About

XBackBone is a simple and lightweight PHP file manager that support the instant sharing tool ShareX and UNIX systems.

* Questions? Ask in the [Cloudron Forum - XBackBone](https://forum.cloudron.io/category/147/xbackbone)
* [XBackBone Website](https://xbackbone.app/)
* [XBackBone forum](https://github.com/SergiX44/XBackBone/discussions)
* [XBackBone issue tracker](https://github.com/SergiX44/XBackBone/issues)

## Registration

Registration is disabled by default. This can be enabled in `System` -> `System Settings`.

## ShareX setup

ShareX can be set up to upload images to a XBackBone instance as follows:

* Download Client configuration from `Profile` -> `Client Configuration` -> `ShareX`.

  <center>
  <img src="/img/xbackbone-sharex-config.png" class="shadow" width="500px">
  </center>

* Upload the client configuration in ShareX. `Destinations` -> `Custom uploader` . Then, `Import` -> `From file`

  <center>
  <img src="/img/xbackbone-sharex-custom-uploader.png" class="shadow" width="500px">
  </center>

* Change the default Image uploader location to your custom uploader.

  <center>
  <img src="/img/xbackbone-sharex-set-uploader.png" class="shadow" width="500px">
  </center>


## File upload size

Change the following values in `/app/data/php.ini` using the [File manager](/apps/#file-manager):

```
post_max_size = 256M
upload_max_filesize = 256M
memory_limit = 256M
```

Be sure to restart the app after making any changes.

