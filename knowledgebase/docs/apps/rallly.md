# <img src="/img/rallly-logo.png" width="25px"> Rallly App

## About

Self-hostable doodle poll alternative. Find the best date for a meeting with your colleagues or friends without the back and forth emails.

* Questions? Ask in the [Cloudron Forum - Rallly](https://forum.cloudron.io/category/154/rallly)
* [Rallly Website](https://rallly.co)
* [Rallly issue tracker](https://github.com/lukevella/rallly/issues)

## Restrict sign-up

To restrict sign-ups, edit `ALLOWED_EMAILS` in `/app/data/env` using the [File manager](/apps/#file-manager).

For example, to restrict only to company email ids:

```
ALLOWED_EMAIL=*@cloudron.io
```

Be sure to restart the app after making any changes.

