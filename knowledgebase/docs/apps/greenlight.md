# <img src="/img/greenlight-logo.png" width="25px"> Greenlight App

## About

Greenlight is a really simple end-user interface for your BigBlueButton server. 

* Questions? Ask in the [Cloudron Forum - Greenlight](https://forum.cloudron.io/category/103/greenlight)
* [Greenlight Website](https://github.com/bigbluebutton/greenlight)
* [Greenlight issue tracker](https://github.com/bigbluebutton/greenlight/issues)

## Installing BigBlueButton

This app is intended to work alongside a BigBlueButton installation. Greenlight is the frontend which
has Cloudron user authentication and the BigBlueButton install is the backend. BBB must be
installed on a separate VM following the instructions in [BigBlueButton documentation](https://docs.bigbluebutton.org/administration/install) and [bbb-install script documentation](https://github.com/bigbluebutton/bbb-install)

To summarize the installation procedure:

* Create a new 64-bit Ubuntu 20.04 server with a public IP and 4G of memory and 4 CPU. Note installation script does not work without these minimum requirements.
* Configure the DNS to point `bbb.example.com` to the public IP below.
* Execute
```
wget -qO- https://raw.githubusercontent.com/bigbluebutton/bbb-install/v2.7.x-release/bbb-install.sh | bash -s -- -w -v focal-270 -s bbb.example.com -e info@example.com
```
* Install Greenlight on Cloudron.
* Take the output of `bbb-conf --secret` on the BigBlueButton server and put these values to the following variables in `/app/data/env` of Greenlight using the [File manager](/apps/#file-manager).
```
BIGBLUEBUTTON_ENDPOINT=
BIGBLUEBUTTON_SECRET=
```
* Restart the Greenlight app.

## User management

### Cloudron Directory

When Cloudron user management is enabled, only Cloudron users can login to greenlight, create and manage meetings.
By default, Cloudron users have `Administrator` role permissions.
This can be changed in `Administrator Panel > Site Settings > Registration`.

### Without Cloudron SSO

When not using Cloudron authentication, there is an Administrator account created with the following credentials at the first run:
```
username: admin@cloudron.local
password: ChangeMe!321
```

## Creating a new user with a role

To create a new User, open a [Web terminal](/apps/#web-terminal) and run the following commands:

```bash
# Creating an admin
bundle exec rake admin:create["admin","admin@server.local","changeme"]

# Information
# bundle exec rake user:create["name","email","password","role"]

# Creating a user
bundle exec rake user:create["user","user@server.local","changeme","User"]
```

