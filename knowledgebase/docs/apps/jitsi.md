# <img src="/img/jitsi-logo.png" width="25px"> Jitsi App

## About

Jitsi Meet is an open source JavaScript WebRTC application used primarily for video conferencing.

* Questions? Ask in the [Cloudron Forum - Jitsi](https://forum.cloudron.io/category/139/jitsi)
* [Jitsi Website](https://jitsi.org/)
* [Jitsi docs](https://jitsi.github.io/handbook/docs/intro)
* [Jitsi issue tracker](https://github.com/jitsi/jitsi/issues)

## Custom configuration

Custom configuration can be set in `/app/data/jitsi-meet-config.js` using the [File manager](/apps/#file-manager).
See the [upstream config](https://github.com/jitsi/jitsi-meet/blob/master/config.js) file for configuration options.

Be sure to restart the app after making any changes.

## Ports

Jitsi uses P2P for conferences with only 2 people.

With more than 2 people, it uses the Jitsi Videobridge for mixing audio/video. Jitsi [requires](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-manual/#running-behind-nat) the port `UDP/10000` to be open for conferences more than 2 people.

