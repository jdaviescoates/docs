# <img src="/img/osticket-logo.png" width="25px"> osTicket App

## About

osTicket is the world’s most popular customer support software.

* Questions? Ask in the [Cloudron Forum - osTicket](https://forum.cloudron.io/category/89/osticket)
* [osTicket Website](https://osticket.com/)
* [osTicket forum](https://forum.osticket.com/)
* [osTicket issue tracker](https://github.com/osTicket/osTicket/issues)

## Admin Checklist

There are a number of email addresses that need to be fixed up before you can start using osTicket.

* Change the administrator email (default: `admin@server.local`) and password. This can be changed under `Agents` tab.

* osTicket adds 3 email addresses by default (listed under `Emails` -> `Email Addresses`. To use the app, you must change the email addresses below and setup their `Outbound (SMTP)` configuration. Without SMTP configuration, osTicket uses phpmailer, which does not work on Cloudron. If mailboxes are hosted on Cloudron, see the [SMTP Configuration](#cloudron-smtp-configuration) section below.

    * `osTicket Alerts <alerts@server.local>` - Email address from which Alerts & Notices are sent to Agents. This can be changed in `Emails` -> `Email Settings and Options` -> `Default Alert Email`.
    * `noreply@server.local` - This is added by default and not used anywhere. Can be removed if you have no use for this.
    * `Support <support@server.local>` - Email address from which outgoing emails are sent. This can be changed in `Emails` -> `Email Settings and Options` -> `Default System Email`.

* Change the Admin email address. This is the email address to which System Errors and New Ticket Alerts (if enabled) are sent. This can be changed under `Emails` -> `Email Settings and Options` -> `Admin's Email Address`. If you miss this, osTicket will send alerts to this address and bounces get attached to tickets.

## User Management

osTicket is integrated with Cloudron user management. However, osTicket does not support
auto creation of user accounts - see [this](https://forum.osticket.com/d/99277-auto-create-user-accounts)
and [this](https://github.com/osTicket/osTicket/issues/1062) for more information.

To workaround agents must be manually added into osTicket before they can login. When adding an agent, choose
LDAP as the authentication backend.

<center>
<img src="/img/osticket-add-agent.png" class="shadow" width="500px">
</center>

## Cloudron SMTP Configuration

osTicket can be configured to process emails from mailboxes hosted with or without Cloudron.

When the mailbox is hosted in Cloudron, you can use the IMAP+SSL at port 993 for receiving Email:

<center>
<img src="/img/osticket-imap.png" class="shadow" width="500px">
</center>

To send email, use port 587:

<center>
<img src="/img/osticket-smtp.png" class="shadow" width="500px">
</center>

## CLI

osTicket comes with a CLI tool for various administrative tasks like managing users.
Use the [Web Terminal](/apps#web-terminal) to run the following command:

```
    sudo -E -u www-data php /app/code/upload/manage.php
```

