# <img src="/img/mirotalk-logo.png" width="25px"> MiroTalk App

## About

Free browser based Real-time video calls. Simple, Secure, Fast.
Start your next video call with a single click. No download, plug-in, or login is required. Just get straight to talking, messaging, and sharing your screen.

* Questions? Ask in the [Cloudron Forum - MiroTalk](https://forum.cloudron.io/category/183/mirotalk)
* [MiroTalk Website](https://p2p.mirotalk.com/)
* [MiroTalk issue tracker](https://github.com/miroslavpejic85/mirotalk/issues)
