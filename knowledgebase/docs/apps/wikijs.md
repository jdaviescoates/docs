# <img src="/img/wikijs-logo.png" style="height: 48px; vertical-align: middle;"> Wiki.js App

## About

Wiki.js is a wiki engine running on Node.js

* Questions? Ask in the [Cloudron Forum - Wiki.js](https://forum.cloudron.io/category/51/wiki-js)
* [Wiki.js Website](https://wiki.js.org/)
* [Wiki.js issue tracker](https://github.com/Requarks/wiki/issues)

## Permissions

When authenticating against the Cloudron Directory, users only have the `Guest` permission (read pages) by default.
Because of this, it might appear that users cannot login.

You can adjust the default permissions of Cloudron users as follows:

* Login as admin
* Create a Group with necessary permissions
* `Authentication` -> `Cloudron` -> `Registration` . Add the group created above to the auto assign groups.

## Git Storage

Wiki.js supports storing documentation in git storage using the [Git module](https://docs.requarks.io/en/storage/git).

### Generate SSH Keys

You can skip this section entirely, if you already have existing SSH keys.

* Open a [Web Terminal](/apps/#web-terminal) and generate the keys in `/app/data/ssh/id_rsa` (see below).
Be sure to leave the passphrase empty as required by Wiki.js. 

```
# ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa): /app/data/ssh/id_rsa
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /app/data/ssh/id_rsa
Your public key has been saved in /app/data/ssh/id_rsa.pub
...

# chown -R cloudron:cloudron /app/data/ssh
```

* Add `/app/data/ssh/id_rsa.pub` as SSH public key into GitHub/GitLab/Gitea/Gogs.

### Create repository directory

Open a [Web Terminal](/apps/#web-terminal) and run the following commands:

```
# mkdir -p /app/data/repo
# chown -R cloudron:cloudron /app/data/repo
```

### Configure Git storage

* Inside Wikijs, go to `Modules` -> `Storage` and select `Git`.

* If you generated the SSH keys as per the instructions above, set the `SSH Private Key Mode` to `path` and set `SSH Private Key Path` to `/app/data/ssh/id_rsa`. Alternately, you can also set this to `contents` and paste in your existing SSH private key.

* Set the Local Repository Path to `/app/data/repo` (created above)

* Newer GitHub repositories use the branch name as `main` instead of `master`. Use the appropriate branch name in Wiki.js config page.

  <center>
  <img src="/img/wikijs-git-settings.png" class="shadow" width="500px">
  </center>

