# <img src="/img/piwigo-logo.png" width="25px"> Piwigo App

## About

Piwigo is open source photo gallery software for the web. Designed for organisations, teams and individuals.

* Questions? Ask in the [Cloudron Forum - Piwigo](https://forum.cloudron.io/category/148/piwigo)
* [Piwigo Website](https://piwigo.org/)
* [Piwigo issue tracker](https://github.com/Piwigo/Piwigo/issues)
