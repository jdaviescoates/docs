# <img src="/img/snipe-it-logo.png" width="25px"> Snipe-IT App

## About

Snipe-IT is a free open source IT asset/license management system.

* Questions? Ask in the [Cloudron Forum - Snipe-IT](https://forum.cloudron.io/category/125/ackee)
* [Snipe-IT Website](https://snipeitapp.com/)
* [Snipe-IT docs](https://snipe-it.readme.io/docs/)
* [Snipe-IT issue tracker](https://github.com/snipe/snipe-it/issues)
* [Snipe-IT Discord](https://discord.gg/yZFtShAcKk)

## Customization

Use the [Web terminal](/apps#web-terminal) to edit custom configuration
under `/app/data/env`.

## Full Company Support

[Full Multiple Companies Support](https://snipe-it.readme.io/docs/general-settings#full-multiple-companies-support)
lets you set up Snipe-IT as a multi-tenant application. This features allows super-admins
to restrict the assets non-super-admins can see. This feature is disabled by default but
can be enabled from the General Settings page.

