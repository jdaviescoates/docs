# <img src="/img/wekan-logo.png" width="25px"> Wekan App

## About

Wekan is an open source Kanban board.

* Questions? Ask in the [Cloudron Forum - Wekan](https://forum.cloudron.io/category/30/wekan)
* [Wekan Website](https://wekan.github.io/)
* [Wekan chat](https://community.vanila.io/wekan)
* [Wekan issue tracker](https://github.com/wekan/wekan/issues)

## API

Wekan has a [REST API](https://github.com/wekan/wekan/wiki/REST-API) for managing users.

## Webbooks

When a webhook is activated, Wekan sends the related information within the POST request body.
See [Webhook-data](https://github.com/wekan/wekan/wiki/Webhook-data) for details.

## Users

Wekan currently does not support syncing or searching users via LDAP. This limitation means
that a user must login to Wekan first before they become available for sharing
boards.

## Admin

To make a Cloudron user an admin, first make sure the user has logged once into Wekan (this
creates the user in Wekan). Then, run the following command after replacing `ADMIN_USERNAME` below
in the [Web Terminal](/apps#web-terminal):

```
# mongosh -u "${CLOUDRON_MONGODB_USERNAME}" -p "${CLOUDRON_MONGODB_PASSWORD}" ${CLOUDRON_MONGODB_HOST}:${CLOUDRON_MONGODB_PORT}/${CLOUDRON_MONGODB_DATABASE} --eval "db.users.update({ username: 'ADMIN_USERNAME' }, { \$set: {isAdmin: true } })"

MongoDB shell version v3.6.3
connecting to: mongodb://mongodb:27017/b52e1fdb-5b2d-417e-a41c-53d060a97141
MongoDB server version: 3.6.3
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
```

## Registration

External user registration is enabled by default. User registration can be disabled from the Wekan's Admin Panel. The Wekan Admin Panel option is only
visible when the Cloudron user is a Wekan administrator.

## Customization

Custom env vars can be added in `/app/data/env` using the [File Manager](/apps/#file-manager). Be sure to restart the app after editing this file.

