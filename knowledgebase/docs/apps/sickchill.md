# <img src="/img/sickchill-logo.png" width="25px"> SickChill App

## About

SickChill is an automatic Video Library Manager for TV Shows.

* Questions? Ask in the [Cloudron Forum - SickChill](https://forum.cloudron.io/category/117/sickchill)
* [SickChill Website](https://sickchill.github.io/)
* [SickChill issue tracker](https://github.com/SickChill/SickChill/issues)

### Configuration

You will have to enable NZB/Torrent providers. You may also want to enable post-processing.

Do *not* enable "Reverse proxy headers" : SickChill will block you from connecting, because it will believe that you are
connecting from a remote address with no passwords, as it does not see Cloudron's authentication wall.

