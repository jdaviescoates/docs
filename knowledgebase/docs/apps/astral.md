# <img src="/img/astral-logo.png" width="25px"> Astral App

## About

Astral is an open source application that allows you to organize your GitHub Stars with ease.

* Questions? Ask in the [Cloudron Forum - Astral](https://forum.cloudron.io/category/120/astral)
* [Astral Website](https://astralapp.com/)
* [Astral issue tracker](https://github.com/astralapp/astral/)

## Setup

Astral requires a Github OAuth Application. You are able to create one from you Github account and then adjust the `env` file to use this application: 

- Go to [Github -> Settings -> OAuth Apps](https://github.com/settings/developers) and create a new OAuth app.
- Enter `Astral`, `astral.yourcloudron.com`, and `https://astral.yourcloudron.com/auth/github/callback` for Application Name, Homepage URL, and Authorization callback URL, respectively. 
- Generate a new Client Secret, make note of this, you'll only see it once. 
- Use the app's [File Manager](/apps#file-manager) to edit `/app/data/env` file. Update the `GITHUB_CLIENT_ID` and `GITHUB_CLIENT_SECRET` values with your Github OAuth App's Client ID and Client Secret. 
- Restart the app.
- Login with Github!

