# <img src="/img/superset-logo.png" width="25px"> Superset App

## About

Apache Superset is a modern data exploration and visualization platform.

* Questions? Ask in the [Cloudron Forum - Superset](https://forum.cloudron.io/category/159/superset)
* [Superset Website](https://superset.apache.org/)
* [Superset issue tracker](https://github.com/apache/superset/issues)

## Demo data

Demo data is a great way to get to know how superset works. To import open the [Web terminal](/apps#web-terminal) into the app and run the following commands:

```sh
source /app/pkg/env.sh
superset fab create-admin --username admin --firstname Superset --lastname Admin --email admin@cloudron.local --password changeme
superset load_examples
```

!!! note "Time and memory consuming"
    Demo data takes many minutes and a lot of memory to get fetched and imported. For this set the memory limit of the app at least to 2Gb

## Custom Config

[Custom configuration](https://superset.apache.org/docs/installation/configuring-superset) can be placed in `/app/data/config/config_user.py`.
For example:

```
FEATURE_FLAGS = {
  'SSH_TUNNELING': True
}
```

Be sure to restart the app after making any changes.

## User Role

The default role for a new user is administrator.

To change this, edit `/app/data/config/config_user.py` using the [File manager](/apps/#file-manager) and change `AUTH_USER_REGISTRATION_ROLE`. For example:

```
AUTH_USER_REGISTRATION_ROLE = "Public"
```

Be sure to restart the app after making any changes.

