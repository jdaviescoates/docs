# <img src="/img/openwebui-logo.png" width="25px"> OpenWebUI App

## About

Open WebUI is an extensible, feature-rich, and user-friendly self-hosted WebUI for various LLM runners, supported LLM runners include Ollama and OpenAI-compatible APIs.

* Questions? Ask in the [Cloudron Forum - OpenWebUI](https://forum.cloudron.io/category/185/openwebui)
* [OpenWebUI Website](https://openwebui.com/)
* [OpenWebUI Docs](https://docs.openwebui.com/)
* [OpenWebUI Community](https://openwebui.com/auth?type=signup)
* [OpenWebUI issue tracker](https://github.com/open-webui/open-webui)

## User management

Registration is enabled by default. However, new users have to be approved by administrator before
they can start using the app. You can disable new registration altogether in the admin settings.

## Ollama

A local [Ollama](https://github.com/ollama/ollama) server is included in the
package and is enabled by default. To disable local Ollama and use a remote one
edit `/app/data/env.sh` using the [File manager](https://docs.cloudron.io/apps/#file-manager).

```
export LOCAL_OLLAMA_ENABLED=false
export OLLAMA_API_BASE_URL="https://<remote-ollama>/api"
```

Be sure to restart the app after making any changes.

### Model Storage

By default, models are stored at `/app/data/ollama-home/models` and are part of the app backups.
Models can be very [large](https://github.com/ollama/ollama?tab=readme-ov-file#model-library)
 and it's probably unnecessary to backup models.

Backup of models can be skipped by changing the storage directory to a Cloudron [volume](https://docs.cloudron.io/volumes/).

* Add a [volume](https://docs.cloudron.io/volumes/#add)
* Add the volume to the app [mounts](https://docs.cloudron.io/apps/#mounts)
* Set the model storage in `/app/data/env.sh` using the [File manager](https://docs.cloudron.io/apps/#file-manager)

```
export OLLAMA_MODELS=/media/ollama-vol/models
```

Be sure to restart the app after making any changes.

### Model Library

Ollama supports a list of models available on ollama.com/library . Sample sizes and memory requirements of
models is available [here](https://github.com/ollama/ollama?tab=readme-ov-file#model-library).

