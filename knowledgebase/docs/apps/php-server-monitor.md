# <img src="/img/phpservermonitor-logo.png" width="25px"> PHP Server Monitor App

## About

PHP Server Monitor is a script that checks whether your websites and servers are up and running.

* Questions? Ask in the [Cloudron Forum - PHP Server Monitor](https://forum.cloudron.io/category/126/php-server-monitor)
* [Website](http://www.phpservermonitor.org/)
* [Issue tracker](https://github.com/phpservermon/phpservermon/issues)

## Check interval

The app is configured to check the status of online servers every 5 minutes and check the status of offline servers every minute.

## Custom config

Custom configuration can be added in `/app/data/config.php` using the [File manager](/apps/#file-manager).

## Public page

To setup a public page (accessed at `/public.php`):

* Set `PSM_PUBLIC` to true in `/app/data/config.php`.
* Create a user named `__PUBLIC__`. Set the Level to `Anonymous`.
* Add servers to user '__PUBLIC__'.
* Go to `/public.php`.

