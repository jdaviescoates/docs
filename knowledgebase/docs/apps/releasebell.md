# <img src="/img/releasebell-logo.png" width="25px"> Release Bell App

## About

Release Bell is a self-hosted release notification service.

* Questions? Ask in the [Cloudron Forum - Release Bell](https://forum.cloudron.io/category/55/release-bell)
* [Release Bell Website](https://releasebell.com/)

## Supported Providers

ReleaseBell supports release notification for repos hosted on the following providers:

* GitHub
* GitLab


