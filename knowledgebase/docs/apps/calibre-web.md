# <img src="/img/calibre-web-logo.png" width="25px"> Calibre Web App

## About

Calibre Web is a web app for browsing, reading and downloading eBooks stored in a Calibre database.

* Questions? Ask in the [Cloudron Forum - Calibre Web](https://forum.cloudron.io/category/105/calibre)
* [Calibre Web Website](https://github.com/janeczku/calibre-web)
* [Calibre Web issue tracker](https://github.com/janeczku/calibre-web/issues)

## Importing existing calibre database

To import an existing Calibre database, do the following:

* Stop the app
* Copy the database into `/app/data/library` using the File Manager
* Start the app

## Library

By default, the library is located at `/app/data/library`. This can be moved to another location
by copying over the existing library to the new cloudron. Note that the library files must have the
owner `cloudron`.

To start with a fresh library is tricky since calibredb offers no straightforward command to create
an empty library. As a workaround, run the following commands using the [Web Terminal](/apps/#web-terminal):

```
# cd /path/to/library
# chown cloudron:cloudron /path/to/library
# gosu cloudron:cloudron calibredb restore_database --really-do-it --with-library /path/to/library
Starting restoring preferences and column metadata ... 0%
Cannot restore preferences. Backup file not found. ... 100%
Restoring database succeeded
old database saved as /path/to/library/metadata_pre_restore.db
```

Switch the library in `Admin` -> `Edit Calibre Database Configuration` :

<center>
<img src="/img/calibre-web-library-location.png" class="shadow" width="500px">
</center>

