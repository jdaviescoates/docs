# <img src="/img/monica-logo.png" width="25px"> Monica App

## About

Monica helps you organize the social interactions with your loved ones.

* Questions? Ask in the [Cloudron Forum - MonicaHQ](https://forum.cloudron.io/category/24/monica)
* [MonicaHQ Website](https://monicahq.com)
* [MonicaHQ issue tracker](https://github.com/monicahq/monica/issues)

## Custom config

[Custom config](https://github.com/monicahq/monica/blob/4.x/.env.example) can be added to `/app/data/env`
using the [File manager](/apps/#file-manager).

Be sure to restart the app after making any changes.

## Multiuser

By default, Monica is setup to be single user.

To enable user registration, open the [File manager](/apps#file-manager) and add
the following variable in `/app/data/env`:

```
APP_DISABLE_SIGNUP=false
```

Restart the app and the login page will show a sign-up link in the login page.

## Mobile App

!!! warning "Mobile App Deprecated"
    The mobile app (Chandler) was [deprecated](https://github.com/monicahq/monica/issues/2275) by the upstream project.

To enable the mobile app, open the [Web terminal](/apps#web-terminal) and run
the following command:

```
$ php artisan passport:client --password --name="MobileApp"
Password grant client created successfully.
Client ID: 8
Client Secret: Q1gM1DXaMUt8rdvU3MhC4dnxGrV2EdjnBfyj9Sjm
```

Now edit the file `/app/data/env` and search/edit the values:

```
PASSPORT_PERSONAL_ACCESS_CLIENT_ID=
PASSPORT_PERSONAL_ACCESS_CLIENT_SECRET=
```

