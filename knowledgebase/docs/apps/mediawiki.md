# <img src="/img/mediawiki-logo.png" width="25px"> MediaWiki App

## About

MediaWiki is a collaboration and documentation platform brought to you by a vibrant community.

* Questions? Ask in the [Cloudron Forum - MediaWiki](https://forum.cloudron.io/category/80/mediawiki)
* [MediaWiki Website](https://www.mediawiki.org)

## Admin

Admin operations can be performed by visiting `Tools` menu on the left > `SpecialPages`.

To change or reset the admin password, open a [Web Terminal](/apps/#web-terminal) and run:

```
php maintenance/run.php changePassword --user=administrator --password=supersecretpassword
```

## Access Control

### Default setup

When using Cloudron SSO, the wiki is setup to be editable only by Cloudron users. Anonymous users can read all pages.

When not using Cloudron SSO, the wiki is setup to be editable by users with a registered wiki account.

### Changing permissions

Use the [File manager](/apps#file-manager) to edit the values in `/app/data/LocalSettings.php`.

Here are some commonly requested settings:

* To disable read access for anonymous users, add the following line:

    $wgGroupPermissions['*']['read'] = false;

* To allow write access to anonymous users, add the following line:

    $wgGroupPermissions['*']['edit'] = true;

* To disable email confirmation before new users are allowed to edit files:

    $wgEmailConfirmToEdit = false;

* To disallow account creation and remove the 'Create account' link:

    $wgGroupPermissions['*']['createaccount'] = false;

## Custom icon

To set a custom icon, use the [File manager](/apps#file-manager) and upload a file named
`/app/data/images/wiki.png`.

## Extensions

### Installing

MediaWiki [extensions](https://www.mediawiki.org/wiki/Manual:Extensions) can be installed
as follows:

* Use the [File manager](/apps#file-manager) to upload the tarball and extract the package under `/app/data/extensions`.
* Change the ownership of the newly uploaded directory to `www-data`
* Load the skin in `/app/data/LocalSettings.php` by adding this line:

```
        wfLoadExtension( '<extension-name>' );
```
* Additional extension settings may be set in `/app/data/LocalSettings.php`

### Suppressing skins

To [suppress](https://www.mediawiki.org/wiki/Manual:Skin_configuration) one or more skins add the following line
to `/app/data/LocalSettings.php`:

```
    $wgSkipSkins = array( "cologneblue", "monobook" );
```

## Skins

### Installing

MediaWiki [skins](https://www.mediawiki.org/wiki/Manual:Gallery_of_user_styles) can be installed
as follows:

* Use the [File manager](/apps#file-manager) to upload the tarball and extract the package under `/app/data/skins`.
* Change the ownership of the newly uploaded directory to `www-data`
* Load the skin in `/app/data/LocalSettings.php` by adding this line:

```
        wfLoadSkin( '<skin-name>' );
```

* The default skin for new users can be changed by adding this line to `/app/data/LocalSettings.php`:

```
        $wgDefaultSkin = '<skin-name>';
```

### Suppressing skins

To [suppress](https://www.mediawiki.org/wiki/Manual:Skin_configuration) one or more skins add the following line
to `/app/data/LocalSettings.php`:

```
    $wgSkipSkins = array( "cologneblue", "monobook" );
```

## Exporting a Wiki

To export in XML format, use the [dumpBackup](https://www.mediawiki.org/wiki/Manual:DumpBackup.php) script as part of MediaWiki
installation. Open a [Web terminal](/apps#web-terminal) and run the following
commands:

```
# cd /app/code/maintenance
# php dumpBackup.php --full > /tmp/dump.xml
```

You can download the dump using the download button at the top of the terminal and entering `/tmp/dump.xml`.

## Importing a Wiki

To import in XML format, use the [importDump](https://www.mediawiki.org/wiki/Manual:ImportDump.php) script as part of MediaWiki
installation.

Open a [Web terminal](/apps#web-terminal):

* Upload the XML using the Upload button
* Run the following commands

```
# cd /app/code/maintenance
# php importDump.php < /tmp/dump.xml
You might want to run rebuildrecentchanges.php to regenerate RecentChanges,
and initSiteStats.php to update page and revision counts
# php rebuildrecentchanges.php
# php initSiteStats.php
```

When importing a wiki, the Main Page might still appear without the correct content. You can fix this by going to the Main Page's History and undoing the latest change. Please note that the administrator account needs a valid email for this to work (preferences -> confirm email address).

