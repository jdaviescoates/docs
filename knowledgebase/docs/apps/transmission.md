# <img src="/img/transmission-logo.png" style="height: 48px; vertical-align: middle;"> Transmission App

## About

Transmission is a fast, easy and free BitTorrent client.

* Questions? Ask in the [Cloudron Forum - Transmission](https://forum.cloudron.io/category/114/transmission)
* [Transmission Website](https://transmissionbt.com/)
* [Transmission forum](https://forum.transmissionbt.com/index.php)
* [Transmission issue tracker](https://github.com/transmission/transmission/issues)

## Download Path

By default, this app is configured to download files into `/app/data/downloads`. You can change this path to a
[volume](/volumes) by editing `/app/data/config/settings.json` and changing `download-dir`.

## Post Processing Script

There is a sample post processing script in `/app/code/transmission/torrentDone.js` . This script, at the end of a download,
automatically hard-link the downloaded files from `/app/data/files/Downloading/` to `/app/data/files/Downloaded/`.
This allows potential post-processing by other apps of the file in `Downloaded`, without interfering with seeding from
the file in `Downloading`. Also, it seeds torrents up to a ratio of 2, then automatically remove them, and removes the
corresponding files from `Downloading`.

The script can be enabled by changing `script-torrent-done-enabled` in in [custom config](#custom-config).

You can also write your own post processing scripts and set `script-torrent-done-filename` accordingly. See https://github.com/transmission/transmission/blob/main/docs/Scripts.md for more information.

## Custom config

Custom configuration can be edited in `/app/data/config/settings.json` using the
[File Manager](/apps/#file-manager). Be sure to restart the app after making changes.

See [transmission docs](https://github.com/transmission/transmission/wiki/Editing-Configuration-Files) for
a full list of configurable options.

