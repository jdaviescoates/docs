# <img src="/img/collabora-logo.png" width="25px"> Collabora Online (CODE)

## About

Collabora is a collaborative online office suite based on LibreOffice technology.

* Questions? Ask in the [Cloudron Forum - Collabora Office](https://forum.cloudron.io/category/57/collabora-office)
* [Collabora Office Website](https://www.collaboraoffice.com)
* [Collabora Office forum](https://forum.collaboraonline.com/)
* [Collabora Office issue tracker](https://github.com/CollaboraOnline/online/issues)

## Setup

The Collabora app can be used to provide rich document editing functionality for
files hosting inside [NextCloud](/apps/nextcloud).

* Install [NextCloud](https://cloudron.io/store/com.nextcloud.cloudronapp.html) from
  the App Store. For this example, we assume NextCloud was installed at `nextcloud.example.com`.

* Install [Collabora](https://cloudron.io/store/com.collaboraoffice.coudronapp.html) from the App Store

* In the Collabora setup UI, provide the domain of the NextCloud installation. If the main domain is the same for both apps, no changes have to be made.

  <img src="/img/collabora-settings.png" class="shadow" width="500px">

* Enable the [Nextcloud Office](https://apps.nextcloud.com/apps/richdocuments) app in NextCloud. This app is under the `Office & text`
  category in the NextCloud app store. Once installed, go to NextCloud `Settings` and
  select the `Office` item on the left pane. Enter the domain of the collabora
  installation.

  <img src="/img/nextcloud-collabora.png" class="shadow" width="500px">

* You should now be able to view and edit rich text documents right inside NextCloud.

  <img src="/img/nextcloud-collabora-editor.png" class="shadow" width="500px">

## Spell check

The empty document templates that are provided by default in Nextcloud are German documents. For
this reason, it might appear that the spell-checker is flagging a lot of spelling errors.

The language of a document can be changed in the footer of the editor.

## Adding fonts

To add custom TTF fonts, place them into `/app/data/fonts` and restart the app.

## Custom config

Configuration can be customized by editing '/app/data/coolwsd.xml` using the [File manager](/apps/#file-manager). Use the [source code](https://github.com/CollaboraOnline/online/blob/master/coolwsd.xml.in) as reference for the values to customize.

Be sure to restart the app after making any changes.

