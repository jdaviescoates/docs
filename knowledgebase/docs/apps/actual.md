# <img src="/img/actual-logo.png" width="25px"> Actual App

## About

Actual is a local-first personal finance tool.

* Questions? Ask in the [Cloudron Forum - Actual](https://forum.cloudron.io/category/181/actual)
* [Actual Website](https://actualbudget.org/)
* [Actual docs](https://actualbudget.org/docs/)
* [Actual issue tracker](https://github.com/actualbudget/actual/issues/)

