# <img src="/img/invoiceninja-logo.png" width="25px"> Invoice Ninja 5 App

## About

InvoiceNinja 5 is the leading self-host platform to create invoices, accept payments, track expenses & time tasks. Support WePay, Stripe, Braintree, PayPal, Zapier, and more!

* Questions? Ask in the [Cloudron Forum - Invoice Ninja](https://forum.cloudron.io/category/11/invoice-ninja)
* [Invoice Ninja Website](https://www.invoiceninja.org)
* [Invoice Ninja forum](https://forum.invoiceninja.com/)

## Customizations

InvoiceNinja [customizations](https://invoiceninja.github.io/en/env-variables/) can be made by opening a [File Manager](/apps#file-manager)
and editing `/app/data/env`.

