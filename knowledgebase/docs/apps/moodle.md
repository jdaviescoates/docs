# <img src="/img/moodle-logo.png" width="25px"> Moodle App

## About

Moodle is the world's most popular learning management system. Start creating your online learning site in minutes! 

* Questions? Ask in the [Cloudron Forum - Moodle](https://forum.cloudron.io/category/102/moodle)
* [Moodle Website](https://moodle.org/)
* [Moodle forum](https://moodle.org/course/)

## Updates

Moodle is a complex application with a complicated upgrade procedure. It supports
over 25 different [types of plugins](https://docs.moodle.org/dev/Plugin_types) each
located in a different location in the source code. While we have automated the upgrade,
do not use any more plugins than necessary to reduce update issues. On the same note,
do not edit the source code of core moodle since it will be overwritten on an update.

## Themes

To install a theme, extract the new theme under `/app/data/moodle/theme` using the
[File manager](/apps/#file-manager). Then, complete the installation by going to
`Site Administration` in Moodle.

