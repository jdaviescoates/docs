# <img src="/img/leantime-logo.png" width="25px"> Leantime App

## About

Leantime is a strategic open source project management system for innovative companies and teams looking to go from start to finish.

* Questions? Ask in the [Cloudron Forum - Leantime](https://forum.cloudron.io/category/171/leantime)
* [Leantime Website](https://leantime.io)
* [Leantime issue tracker](https://github.com/Leantime/leantime/issues)
