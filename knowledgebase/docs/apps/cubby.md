# <img src="/img/cubby-logo.png" width="25px"> Cubby App

## About

Cubby is a pure filesharing app with built-in viewers. It further supports an external collabora office installation.

* Questions? Ask in the [Cloudron Forum - Cubby](https://forum.cloudron.io/category/132/cubby)
* [Cubby Website](https://getcubby.org/)
* [Cubby issue tracker](https://github.com/cloudron-io/cubby/issues)

