# <img src="/img/couchpotato-logo.png" width="25px"> CouchPotato App

## About

CouchPotato automatically find movies you want to watch.

* Questions? Ask in the [Cloudron Forum - CouchPotato](https://forum.cloudron.io/category/118/couchpotato)
* [CouchPotato Website](https://couchpota.to/)
* [CouchPotato Forum](https://couchpota.to/forum/)

