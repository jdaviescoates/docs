# <img src="/img/onlyoffice-logo.png" width="25px"> ONLYOFFICE App

## About

ONLYOFFICE has to be integrated with some the document store. On Cloudron there is currently Nextcloud available as a
document store application, other [3rdparty solutions](https://www.onlyoffice.com/all-connectors.aspx) are also supported.

* Questions? Ask in the [Cloudron Forum - ONLYOFFICE Docs](https://forum.cloudron.io/category/13/onlyoffice)
* [ONLYOFFICE Website](https://www.onlyoffice.com/)
* [ONLYOFFICE forum](https://dev.onlyoffice.org/)
* [ONLYOFFICE Docs issue tracker](https://github.com/ONLYOFFICE/CommunityServer/issues)

## Changing default app secret

The default secret for the ONLYOFFICE app package in Cloudron is `changeme`. Please change that to some unique secret:

* Open a [File Manager](/apps/#file-manager) into the app

* Edit the file `/app/data/config/production-linux.json`

  * Locate the section called `secret`.
```
              "secret": {
                "inbox": {
                  "string": "changeme"
                },
                "outbox": {
                  "string": "changeme"
                }
```

   * Be sure to change the **two secrets** above to the same unique password.

* Restart the app

## Setup Nextcloud connector

!!! warning "Do not install the Document Server"
    There are two ONLYOFFICE apps - [Community Document Server](https://apps.nextcloud.com/apps/documentserver_community) and
    [ONLYOFFICE](https://apps.nextcloud.com/apps/onlyoffice). Be sure to install the latter.

To integrate ONLYOFFICE into Nextcloud for office document editing and collaboration, install [ONLYOFFICE](https://apps.nextcloud.com/apps/onlyoffice) from the Nextcloud app library and configure the plugin as follows, adjusting the domain and secret:

<img src="/img/onlyoffice-nextcloud-integration.png" class="shadow" width="500px">

If your Nextcloud has a self-signed certificate, you should also add the following stanza under `services` -> `CoAuthoring`  in `/app/data/config/production-linux.json` using the [File manager](/apps/#file-manager). Be sure to restart the app after adding the section below:

```
  "services": {
    "CoAuthoring": {
      "requestDefaults": {
        "rejectUnauthorized": false
      },
      ...
    }
  }
```

## Custom Fonts

ONLYOFFICE supports adding custom fonts. Place the TTF files in `/app/data/fonts/` via the [File Manager](/apps/#file-manager),
then restart the app and the browser cache has to be cleared for the frontend to pickup the new fonts.

## Enterprise License

OnlyOffice on Cloudron comes in two editions. Install the enterprise edition if an OnlyOffice enterprise license should be used.
A license can be purchased [here](https://www.onlyoffice.com/docs-enterprise-prices.aspx).

The license key has to be put in the app at `/app/data/license.lic` and the app needs to be restarted then to pick it up.

