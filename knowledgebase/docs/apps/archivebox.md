# <img src="/img/archivebox-logo.png" width="25px"> ArchiveBox App

## About

ArchiveBox is a powerful, self-hosted internet archiving solution to collect, save, and view websites offline.

* Questions? Ask in the [Cloudron Forum - ArchiveBox](https://forum.cloudron.io/category/182/archivebox)
* [ArchiveBox repo](https://github.com/ArchiveBox/ArchiveBox)
* [ArchiveBox issue tracker](https://github.com/ArchiveBox/ArchiveBox/issues)
* [ArchiveBox wiki](https://github.com/ArchiveBox/ArchiveBox/wiki)

## Troubleshooting

If it fails to create screenshots, pdf, html dom, you can run it manually in the [Web terminal](/apps#web-terminal):

```
$ cd /app/data/archivebox/
$ /app/data/archivebox# gosu cloudron:cloudron archivebox update -t timestamp YOUR_TIMESTAMP
```

Where `YOUR_TIMESTAMP` is a value taken from the snapshot page.

Or you can update all snapshots by running:

```
$ cd /app/data/archivebox/
$ gosu cloudron:cloudron archivebox update
```
