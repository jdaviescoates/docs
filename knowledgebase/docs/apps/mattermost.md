# <img src="/img/mattermost-logo.png" width="25px"> Mattermost App

## About

Mattermost is an open source, self-hosted Slack-alternative.

* Questions? Ask in the [Cloudron Forum - Mattermost](https://forum.cloudron.io/category/26/mattermost)
* [Mattermost Website](https://mattermost.org/)
* [Mattermost forum](https://forum.mattermost.org/)
* [Mattermost docs](https://docs.mattermost.com/index.html)
* [Mattermost issue tracker](https://github.com/mattermost/mattermost-server/issues)

## Config

The config file is located at `/app/data/config.json` and can be edited using the [File manager](/apps/#file-manager).
Be sure to restart the app after making changes to the config file.

## Command Line Tool

The [Mattermost CLI tool](https://docs.mattermost.com/manage/mmctl-command-line-tool.html) can be used
to administer user and team management tasks.

Starting Mattermost 6.0, the CLI tool has to installed locally on your Mac/PC. See the [CLI docs](https://docs.mattermost.com/manage/mmctl-command-line-tool.html#install-mmctl) for downloading the binaries.

Once installed, you can run mmctl like so:

```
$ /app/code/mmctl auth login https://mattermost.cloudron.space --name myserver --username admin
Password:

  credentials for "myserver": "admin@https://mattermost.cloudron.space" stored
```

You can then run commands like this `mmctl team list` etc.

## Migration

In you want to migrate your existing non-Cloudron mattermost installation to Cloudron, do
the following:

* [Export data](https://docs.mattermost.com/administration/bulk-export.html#bulk-export-data) from the old
  installation as follow:

```
$ /path/to/mattermost export bulk all.zip --all-teams --attachments --archive
```

* Install mattermost on Cloudron. By default, Cloudron uses the `team` edition of mattermost. Switch to [Enterprise](#enterprise) if required.

* Then use the [Web Terminal](/apps#web-terminal) to first upload  the `all.zip` above to the `/tmp` directory using the Upload button in the
  top of the Web terminal. Then, import it using the following command:

```
$ unzip /tmp/all.zip

$ mm_edition=team # can also be set to 'enterprise'

$ sudo -u cloudron /app/code/$mm_edition/bin/mattermost --config=/app/data/config.json import bulk --apply /tmp/import.jsonl --import-path /tmp/data
```

## Enterprise

To switch to the Enterprise version of Mattermost, edit `/app/data/edition.ini` using the [File manager](/apps/#file-manager)
and change `edition=enterprise`.

Be sure to restart the app after making any changes.
