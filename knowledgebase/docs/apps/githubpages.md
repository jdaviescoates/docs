# <img src="/img/githubpages-logo.png" width="25px"> GitHub Pages App

## About

GitHub pages compatible repos can be published using this app.

* Questions? Ask in the [Cloudron Forum - GitHub Pages](https://forum.cloudron.io/category/39/github-pages)
* [GitHub Pages Website](https://pages.github.com/)
* [Upstream GitHub Pages forum](https://forum.cloudron.io/category/39/github-pages)
* [Upstream GitHub Pages issue tracker](https://forum.cloudron.io/category/39/github-pages)

## Publishing pages

To publish your page, push your GitHub Pages repository via HTTP to this
app:

```
    git remote add page https://<app.example.com>/_git/page
    git push page master
```

When pushing, `git` will prompt for Cloudron username and credentials. Any
Cloudron user with access to the app can push.

It can be convenient to store the HTTP username and password in the `~/.netrc`
file:

```
    machine app.example.com login fred password bluebonnet
```

Adjust `app.example.com`, `fred` and `bluebonnet` above to your setup.

## Build Branch

By default, the app renders the `master` branch. To change this, edit `/app/data/env.sh` using the [File manager](/apps/#file-manager):

```
export BUILD_BRANCH=main
```

Be sure to restart the app, after making any changes.

## Jekyll

The app uses the [pages gem](https://github.com/github/pages-gem) to statically build the website.

Limitations:

* Only Jekyll 3 is supported. For Jekyll 4, you must build the site manually.
* Only the plugins listed in the [pages plugins](https://pages.github.com/versions/) page are supported.

## Using mkdocs

[mkdocs](https://www.mkdocs.org/) has a command called `gh-deploy` that can
automatically build docs and publish the site to a specific remote and branch.

To force skip of Jekyll build, create a file name `.nojekyll` at the root of the repo.

```
    git remote add page https://site.cloudron.xyz/_git/page # add the github-pages app remote
    mkdocs gh-deploy --remote-name page --remote-branch master --force
```

## Jekyll 4

This app only supports automatic build of Jekyll 3 websites. To publish a website with Jekyll 4:

* Install Jekyll locally with `sudo gem install jekyll`.
* Build the site locally on your laptop with `jekyll build`.
* Add the `_site` to your git repo.
* Add a `.nojekyll` at the root of the repo.
* Push to the pages app.

