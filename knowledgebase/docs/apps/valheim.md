# <img src="/img/valheim-logo.png" width="25px"> Valheim App

## About

This app packages the Valheim Dedicated Server

* Questions? Ask in the [Cloudron Forum - Valheim](https://forum.cloudron.io/category/128/valheim)
* [Valheim Game Website](https://www.valheimgame.com/)

## Mod Support

If `MODSUPPORT` is enabled, all files and folders from `/app/data/mods/` will by synced to `/run/vhserver-steam/`.

Place your Mod `*.dll` files in `/app/data/mods/BepInEx/plugins/` and the Mod Config files in `/app/data/mods/BepInEx/config/`

### Mod Example [ValheimPlus](https://github.com/valheimPlus/ValheimPlus)

The [UnixServer.zip](https://github.com/valheimPlus/ValheimPlus/releases/download/0.9.6/UnixServer.zip) of [ValheimPlus](https://github.com/valheimPlus/ValheimPlus) has the same folder structure which cold be extracted into `/app/data/mods/` but this would also overwrite the cloudron auto installed version of BepInEx.
If you make a mistake here, don't worry. Simply clear the `/app/data/mods/` folder and it will be recreated after a app restart.
