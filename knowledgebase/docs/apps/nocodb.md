# <img src="/img/nocodb-logo.png" width="25px"> NocoDB App

## About

NocoDB is an open source #NoCode platform that turns any database into a smart spreadsheet.

* Questions? Ask in the [Cloudron Forum - NocoDB](https://forum.cloudron.io/category/140/nocodb)
* [NocoDB website](https://nocodb.com/)
* [NocoDB docs](https://docs.nocodb.com/)
* [NocoDB issue tracker](https://github.com/nocodb/nocodb/issues)

## Custom config

Custom configuration can be configured via environment variable. See [upstream docs](https://docs.nocodb.com/getting-started/installation/#environment-variables) for more information.

## Telemetry

As per upstream default, telemetry is enabled by default. You can disable this by setting the following:

```
export NC_DISABLE_TELE=true
```

## Email

* Go to Account Settings as the Admin, then to the App Store page
* Install and configure the `SMTP` app. If you are using Cloudron Email, create a mailbox and configure it here. Use settings similar to below:

<center>
<img src="/img/nocodb-smtp.png" class="shadow" width="500px">
</center>

