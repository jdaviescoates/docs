# Users & Groups

## Overview

Cloudron provides a central user directory that apps can use for authentication. This feature
allows users to use the same username & password for logging in to apps.

When installing an app, you can choose if the app is to be configured to use the Cloudron user directory.
Disabling the integration can be beneficial when the app is meant to be used primarily by external users
(for example, a community chat or a public forum).

When Cloudron user directory integration is available for an app, the user management options will look like below:

<center>
<img src="/img/users-directory-integration-available.png" class="shadow" width="500px">
</center>

When `Leave user management to the app` is selected, the app's Cloudron SSO integration will be disabled
and all user management has to be carried from within the app. This is useful when the app primarily caters
to external users (like say a community chat).

When Cloudron user directory integration is unavailable for an app, the user management options look like below:

<center>
<img src="/img/users-directory-integration-unavailable.png" class="shadow" width="500px">
</center>

## Users

New users can be added with their email address from the `Users` menu.

<center>
<img src="/img/user-list.png" class="shadow" width="500px">
</center>

Click on `New User` to add a new user:

<center>
<img src="/img/users-add.png" class="shadow" width="500px">
</center>

They will receive an invite to sign up. Once signed up, they can access the apps they have been given access to.

To remove a user, simply remove them from the list. Note that the removed user cannot access any app anymore.

### Valid usernames

The following characters are allowed in usernames:

* Alphanumeric characters
* '.' (dot)
* '-' (hyphen)

Usernames must be chosen with care to accomodate the wide variety of apps that run on Cloudron.
For example, very generic words like `error`, `pull`, `404` might be reserved by apps.

## Groups

Groups provide a convenient way to group users. You can assign one or more groups to apps to restrict who can
access for an app.

You can create a group by using the `Groups` menu item.

<center>
<img src="/img/users-groups-list.png" class="shadow">
</center>

Click on `New Group` to add a new group:

<center>
<img src="/img/users-groups-add.png" class="shadow">
</center>


To set the access restriction use the app's configure dialog.

<center>
<img src="/img/app-configure-group-acl.png" class="shadow" width="500px">
</center>

### Valid group names

The following characters are allowed in group names:

* Alphanumeric characters
* '.' (dot)
* '-' (hyphen)

## Roles

Roles provide a way to restrict the permissions of a user. You can assign a role from the `Users` page.

<center>
<img src="/img/users-role.png" class="shadow" width="500px">
</center>

### User

A Cloudron user can login to the Cloudron dashboard and use the apps that they have access to.
They can edit their profile (name, password, avatar) on the dashboard.

To allow a user to configure and manage specific apps, see the [App Operator](/apps/#operators) feature.

### User Manager

A User Manager can add, edit and remove users & groups. Newly added users always get the `User` role.
User Manager cannot modify the role of an existing user.

### Mail Manager

A Mail Manager can add, edit and remove mailboxes and mailing lists, in addition to being able to manage users.

!!! note "No access to mail server logs"
    For security reasons, a Mail Manager does not have access to the email server logs.

### Administrator

A Cloudron administrator can manage apps and users. An admin can:

* Login to any app even if they have not explicitly granted access to in the `Access Control` section
* [Impersonate any user](#impersonate-user)
* Access data of other users via [File Manager](/apps/#file-manager) or [Web Terminal](/apps/#web-terminal)
* Configure various aspects of Cloudron like branding, networking, domains, services etc
* Access mail server logs

If you only want to make a user configure and manage specific apps, use the [App Operator](/apps/#operators) feature.

### Superadmin

A Cloudron superadmin has all the capabilities of administrator. In addition, a superadmin can:

* Manage the Cloudron subscription
* Manage backup storage and policy
* Open support tickets

A good way to think about the superadmin role is a person who is in charge of server administration and billing.

!!! note "Automatic login"
    When clicking the `Manage Subscription` button in the `Settings` view, they are automatically logged
    in to the cloudron.io account.

## Impersonate user

A common situation is to be able to pre-setup applications on behalf of a new user. In some cases, the user has
to login to an app first before they can be added to a channel/group/team or given specific permissions.

For such situations, the Cloudron admin can use the `Impersonate` button to generate a password that lets a Cloudron
login as another user. The password is time limited and can be used to login to the Cloudron dashboard and the apps.

<center>
<img src="/img/users-impersonate-button.png" class="shadow" width="500px">
</center>

Clicking the button will show a temporary password:

<center>
<img src="/img/users-impersonate-password.png" class="shadow" width="500px">
</center>

!!! note "Does not reset existing password"
    The temporary password does not overwrite the user's existing password.

## Password reset (link)

Users & Admins can reset their own passwords from the login screen. `https://my.example.com/login.html?passwordReset` is a direct link.

Admins can also email a password reset link for other users by clicking on the `Reset password` button.

<center>
<img src="/img/users-password-reset.png" class="shadow" width="500px">
</center>

This will open up a dialog showing the password reset link. If email delivery is not working for some
reason, the link can be sent to the user by some other means.

<center>
<img src="/img/users-copy-password-reset-link.png" class="shadow" width="500px">
</center>

## Password reset (ssh)

A temporary one-time use password for the superadmin account can be generated by SSHing into the server:

```
# sudo cloudron-support --owner-login
Login as superadminname / mW5x5do99TM2 . Remove /home/yellowtent/platformdata/cloudron_ghost.json when done.
```

The above password also bypasses any 2FA and can thus be used in situations where the superadmin has lost the 2FA device.

## Disable 2FA

If a user loses their 2FA device, the Cloudron administrator can disable 2FA
in the user's edit dialog.

<center>
<img src="/img/users-2fa-reset.png" class="shadow" width="500px">
</center>

Once disabled, user can login with just their password. After login, they can
re-setup 2FA.

If the superadmin of Cloudron has lost their 2FA device, see the [password reset](#password-reset-ssh)
section to generate a one-time use password that will bypass 2FA.

## Disable user

To disable a user, uncheck the `User is active` option. Doing so, will invalidate all
existing Cloudron dashboard session of the user and will log them out. The user may
still be able to use any apps that they were logged into prior to the de-activation.
To log them out from the apps, you can check if the app provides a way to log them out
(support for this depends on the app).

<center>
<img src="/img/user-disable.png" class="shadow" width="500px">
</center>

!!! note "Disabling does not delete user data"
    Disable a user only blocks the login access for the user. Any data generated by the
    user inside apps is not deleted.

## Impersonate user

One can create a file named `/home/yellowtent/platformdata/cloudron_ghost.json` which contains an username
and a fake password like:

```
{"girish":"secret123"}
```

With such a file in place, you can login to the Webadmin UI using the above credentials
(the user has to already exist). This helps you debug and also look into how the UI might
look from that user's point of view.

