FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

# passed from ./update.sh
ARG MKDOCS_MATERIAL_VERSION
ARG MKDOCS_REDIRECTS_VERSION
ARG SURFER_VERSION

RUN apt-get update && \
    apt install -y python3-setuptools && \
    pip3 install mkdocs-material==$MKDOCS_MATERIAL_VERSION && \
    pip3 install mkdocs-redirects==${MKDOCS_REDIRECTS_VERSION} && \
    pip3 install pillow cairosvg && \
    npm i -g @redocly/cli && \
    npm install -g cloudron-surfer@$SURFER_VERSION
